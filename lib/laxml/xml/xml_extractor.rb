require 'ox'

module Laxml
  module XML
    class XmlExtractor

      def self.extract(source)
        return Ox.parse(source)
      end

    end
  end
end
