require 'ox'

module Laxml
  module XML
    class XmlLoader

      def self.load(object)
        return Ox.dump(object)
      end

    end
  end
end
