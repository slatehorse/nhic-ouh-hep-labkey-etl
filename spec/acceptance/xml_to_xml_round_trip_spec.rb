require 'ox'
require 'pathname'
require 'pry'

require 'active_support'
require 'active_support/dependencies'
relative_load_paths = %w[lib]
ActiveSupport::Dependencies.autoload_paths += relative_load_paths

require 'active_support/core_ext/hash'

Ox.default_options = {:encoding => 'utf-8'}


SAMPLE_XML_ROOT = Pathname.new('spec').join('sample_xml')

RSpec.describe 'XML to XML round trip', :acceptance=>true do

  [
    'instance_randomised1.xml',
    # 'instance_randomised2.xml',
    # 'instance_randomised3.xml',
    # 'instance_randomised4.xml',
    # 'instance_randomised5.xml',
  ].each do | sample_xml_filename |

    before(:each) do
      @input_file = SAMPLE_XML_ROOT.join(sample_xml_filename)
    end

    # Test round trip
    before(:each) do
      extracted_object = Laxml::XML::XmlExtractor.extract(@input_file.read)
      generated_xml = Laxml::XML::XmlLoader.load(extracted_object)
      @roundtripped_hash = Hash.from_xml(generated_xml);

      @source_hash = Hash.from_xml(@input_file.read);
    end

    it "Generates semantically identical XML to the input" do
      expect( @source_hash ).to eq( @roundtripped_hash )
    end

    it "isn't empty" do
      expect( @roundtripped_hash ).not_to be_nil
    end
  end
end


